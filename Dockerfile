FROM docker.io/archlinux:base

RUN pacman -Syu --noconfirm \
	make \
	openocd \
	arm-none-eabi-gcc \
	arm-none-eabi-gdb \
	arm-none-eabi-newlib \
	arm-none-eabi-binutils
VOLUME /build
EXPOSE 3333 4444
