# archlinux-arm-toolchain

Tools for compiling, flashing, and debugging ARM microcontrollers.

## What's included

The container has the `arm-none-eabi` toolchain installed, as well as OpenOCD.
OpenOCD can use devices on the host system to flash and debug in circuit. This
requires passing the device through to the container, which depends on the debugging device.
